## @package make3Dskeleton
#  3D skeleton from openpose keypoints and obj model
#
#  This script expects
#
#  the directory containing the scan
#  ${modelDir}
#    │ 
#    │   rgb texture from camera
#    ├── ${cameraSerial}_tex_Color.png
#    │ 
#    │   obj model with vertices and texture coordinates
#    │   grouped per camera. groupname group${cameraSerial}
#    │      # example obj file
#    │ 
#    │      mtllib material.mtl  [.. ignored ..]
#    │      o human  [.. ignored ..]
#    │ 
#    │      usemtl mtl_822512060753 [.. ignored ..]
#    │      g group822512060753
#    │      v -0.097449 0.076481 -0.581530 [.. color ignored ..]
#    │      vt 0.457288 0.014672
#    │      ...
#    │ 
#    └── ${modelFilename}
#
#  ${keypointsDir}
#    │ 
#    │   keypoints from openpose
#    │   adjust format in settings
#    ├── ${cameraSerial}_tex_Color_keypoints.json
#    │ 
#    │   rgb texture from camera as used by openpose
#    │   not processed by script, check for rotation
#    │   and adjust in settings["keypointsRotation"] in degrees
#    │   check for dimensions
#    │   and adjust in settings["keypointsSourceDimensions"] in pixels
#    └── ${cameraSerial}_tex_Color_rendered.png
#
##################### imports
import json
from pathlib import Path
import glob
import shutil
import subprocess
import os
import numpy as np
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import pprint
import time

from BODY25_definition import KeypointEnum, skeletonToBvh 

##################### settings
# store as dict, so we could load it from json
settings = {
    "modelDir": "./models/2019.01.17_11:35:48",
    "modelFilename": "model_8.obj",
    "openposeImagesDir": "./images",
    "keypointsDir": "./output",
    "keypointsFormat": "BODY_25",
    "keypointsSourceDimensions": {
        "width": 1080,
        "height": 1920
    },
    "keypointsRotation": 270
}

##################### classes
######### utility
class col:
    Red = '\033[91m'
    Green = '\033[92m'
    Blue = '\033[94m'
    Cyan = '\033[96m'
    White = '\033[97m'
    Yellow = '\033[93m'
    Magenta = '\033[95m'
    Grey = '\033[90m'
    Black = '\033[90m'
    Default = '\033[99m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

######### specific
class SkeletonVertex:
    def __init__(self, vertex=np.array([0,0,0]), confidence=0):
        self.vertex = vertex
        self.confidence = confidence
    def __repr__(self):
        return f"{self.vertex} {self.confidence}"
    def __str__(self):
        return f"{self.vertex} {self.confidence}"

##################### functions
######### utility
def loadJsonFile(file):
    print(f"{col.Yellow}load json {file}{col.ENDC}")
    with open(file, 'r') as f:
        data=f.read()
    return json.loads(data)

def getCamerasFromDirectory(dir):
    print(f"{col.Yellow}get cameras from {dir}{col.ENDC}")
    cameras = []
    pathlist = Path(dir).glob("*_tex_Color.png")
    for path in pathlist:
         # because path is object not string
         path_in_str = str(path)
         splitter = path_in_str.split("/")
         filename = splitter[len(splitter)-1]
         camera_pp = filename.split("_")[0]
         camera_pps = camera_pp.split("/")
         camera = camera_pps[len(camera_pps)-1]
         cameras.append(camera)
    print(f"{col.Yellow}getCamerasFromDirectory found {len(cameras)} cameras{col.ENDC}")
    return cameras

def getKeypointsFromCamera(dir,camera):
    print(f"{col.Yellow}get keypoints from camera {camera}{col.ENDC}")
    filePath = "{}/{}_tex_Color_keypoints.json".format(dir,camera)
    keypoints = loadJsonFile(filePath)["people"][0]["pose_keypoints_2d"]
    return keypoints

def getCurrentCameraAndChanged(line,cameras,previousCamera):
    cameraChanged = False
    currentCamera = previousCamera
    for c in range(len(cameras)):
        camera = cameras[c]
        if (line.find("group{}".format(camera)) != -1):
            cameraChanged = True
            currentCamera = camera
    return currentCamera, cameraChanged

## getCameraVerticesDict
#
# return dictionary
# {
#   "cameraSerial": {
#     "v": [],
#     "vt": [],
#     "vn": []
#   }
# }
#
def getCameraVerticesDict(dir,cameras):
    print(f"{col.Yellow}get vertices dictionary for all cameras{col.ENDC}")
    verticesDict = {}
    filepath = "{}/{}".format(dir,settings["modelFilename"])
    print("model filepath: {}".format(filepath))
    with open(filepath) as f:
            lines = f.readlines()
    currentCamera = "ERROR no camera found"
    nVertices = 0
    nColors = 0
    nTextureCoordinates = 0
    nNormals = 0
    for i in range(len(lines)):
        line = lines[i]
        currentCamera, changed = getCurrentCameraAndChanged(line,cameras,currentCamera)
        if changed and not hasattr(verticesDict, currentCamera):
            verticesDict[currentCamera] = {}
            verticesDict[currentCamera]["v"] = []
            verticesDict[currentCamera]["vt"] = []
            verticesDict[currentCamera]["vn"] = []
            verticesDict[currentCamera]["color"] = []
            verticesDict[currentCamera]["joints"] = []
            verticesDict[currentCamera]["confidence"] = []
        if (line.find("v ") == 0):
            split = line.split(" ")
            vertex = np.array([float(split[1]),float(split[2]),float(split[3])])
            verticesDict[currentCamera]["v"].append(vertex)
            verticesDict[currentCamera]["joints"].append("")
            verticesDict[currentCamera]["confidence"].append(0)
            nVertices = nVertices + 1
            if (len(split) >= 7):
                color = np.array([float(split[4]),float(split[5]),float(split[6])])
                verticesDict[currentCamera]["color"].append(color)
                nColors = nColors + 1
        if (line.find("vt ") == 0):
            split = line.split(" ")
            texCoord = np.array([float(split[1]),float(split[2])])
            verticesDict[currentCamera]["vt"].append(texCoord)
            nTextureCoordinates = nTextureCoordinates + 1
        if (line.find("vn ") == 0):
            split = line.split(" ")
            normal = np.array([float(split[1]),float(split[2]),float(split[3])])
            verticesDict[currentCamera]["vn"].append(normal)
            nNormals = nNormals + 1

    # naively verify that there were no crude errors
    if (nVertices != nTextureCoordinates):
        print(f"{col.Red}ERROR: nVertices != nTextureCoordinates{col.ENDC}")
        exit(1)
    elif nNormals > 0 and (nVertices != nNormals):
        print(f"{col.Red}ERROR: nVertices != nNormals{col.ENDC}")
        exit(1)
    elif nColors > 0 and (nVertices != nColors):
        print(f"{col.Red}ERROR: nVertices != nColors{col.ENDC}")
        exit(1)
    else:
        print(f"{col.Green}loaded {nVertices} vertices and texCoords{col.ENDC}")

    return verticesDict

def getSingleCameraSkeleton(keypoints,vertices,camera):
    print(f"{col.Yellow}get single camera skeleton for camera {camera}{col.ENDC}")
    # the amount of keypoints is the array divided by three (x, y, confidence)
    nKeypoints = int(len(keypoints) / 3)
    if (len(keypoints) % 3 != 0):
        print(f"{col.Red}Error : getSingleCameraSkeleton : keypoints amount is not dividable by 3 {col.ENDC}")
        exit(1)

    skeleton = []

    for i in range(nKeypoints):
        x = keypoints[i * 3]
        y = keypoints[i * 3 + 1]
        c = keypoints[i * 3 + 2]

        # only process, if there is a keypoint found
        if (c == 0):
            print(f"{col.Grey}no keypoint found for {KeypointEnum(i)} x:{x} y:{y} c:{c}{col.ENDC}")
            skeleton.append(SkeletonVertex())
            continue

        # x and y are in pixels relating to the source image
        # to get the position as we need it from 
        sourceDimensions = settings["keypointsSourceDimensions"]
        if (settings["keypointsRotation"] == 0):
            rel = np.array([
                    x/sourceDimensions["width"],
                    y/sourceDimensions["height"]
                ])
        elif (settings["keypointsRotation"] == 270):
            rel = np.array([
                    1-(y/sourceDimensions["height"]),
                    (x/sourceDimensions["width"])
                ])
        else:
            print(f"{col.Red}ERROR : getSingleCameraSkeleton : keypointsRotation not implemented{col.ENDC}")
            exit(1)

        closestV = -1
        closestDistance = float("inf")

        for v in range(len(vertices["v"])):
            vt = vertices["vt"][v]
            distance = np.sqrt(np.sum((rel - vt) ** 2))
            if (distance < closestDistance):
                closestV = v
                closestDistance = distance

        skeletonVertex = SkeletonVertex(vertices["v"][closestV],c)
        skeleton.append(skeletonVertex)

        vertices["joints"][closestV] = f"{KeypointEnum(i)}".replace("KeypointEnum.","")
        vertices["confidence"][closestV] = c
        print(f"{col.Green}keypoint found {skeletonVertex} for {KeypointEnum(i)}{col.ENDC}")
        
    return skeleton

def getUeberSkeleton(skeletons):
    print(f"{col.Yellow}getUeberSkeleton{col.ENDC}")
    # retrieve the amount of keypoints from the first skeleton
    nKeypoints = len(skeletons[list(skeletons.keys())[0]])

    out = []
    for i in range(nKeypoints):
        x = []
        y = []
        z = []
        c = []
        averageConfidence = 0
        nConfidences = 0
        for camera, skeleton in skeletons.items():
            if skeleton[i].confidence != 0:
                x.append(skeleton[i].vertex[0])
                y.append(skeleton[i].vertex[1])
                z.append(skeleton[i].vertex[2])
                c.append(skeleton[i].confidence)
            averageConfidence += skeleton[i].confidence
            nConfidences += 1
        #TODO: here we just weight by confidence, but
        #      we could also move in the direction of
        #      a possibly smoothed surface normal
        #      to find the spatial position of a joint
        vertex = np.array([
                np.average(x,weights=c),
                np.average(y,weights=c),
                np.average(z,weights=c),
            ])
        averageConfidence /= nConfidences
        out.append(SkeletonVertex(vertex,averageConfidence))

    return out

def plotSkeleton(skeleton):
    data = []
    for i in range(len(skeleton)):
        v = skeleton[i].vertex
        data.append({
            'x':v[0],
            'y':v[1],
            'z':v[2],
            'c':skeleton[i].confidence,
            'name':f"{KeypointEnum(i)}".replace("KeypointEnum.","")
            })
    plot = px.scatter_3d(data,x="x",y="y",z="z",color="c",text="name")
    plot.show()

def plotModel(verticesDict):
    data = []
    for i in range(len(verticesDict["v"])):
        v = verticesDict["v"][i]
        vt = verticesDict["vt"][i]
        vn = verticesDict["vn"][i]
        color = verticesDict["color"][i]
        rgb = f"rgb({round(color[0]*255)}, {round(color[1]*255)}, {round(color[2]*255)})"
        joint = verticesDict["joints"][i]
        data.append({
            'x':v[0],
            'y':v[1],
            'z':v[2],
            'vt':vt.tolist(),
            'vn':vn.tolist(),
            'color':1 if joint == "" else 0,
            'size':1 if joint == "" else 2,
            'joint':joint
            })
    plot = px.scatter_3d(data,x="x",y="y",z="z",color="color",text="joint",size="size")
    plot.show()

def plotModelAndSkeleton(allVerticesDict, skeleton, cameras):
    data = {"x":[],"y":[],"z":[],"color":[],"joints":[],"size":[],"vt":[],"vn":[],"text":[]}
    for c in range(len(cameras)):
        camera = cameras[c]
        verticesDict = allVerticesDict[camera]
        for i in range(len(verticesDict["v"])):
            v = verticesDict["v"][i]
            vt = verticesDict["vt"][i]
            vn = verticesDict["vn"][i]
            color = verticesDict["color"][i]
            joint = verticesDict["joints"][i]
            data["x"].append(verticesDict["v"][i][0])
            data["y"].append(verticesDict["v"][i][1])
            data["z"].append(verticesDict["v"][i][2])
            data["color"].append(0)# if joint == "" else verticesDict["confidence"][i]+1)
            data["size"].append(5 if joint == "" else 10)
            data["joints"].append(joint)
            data["vt"].append(vt.tolist())
            data["vn"].append(vn.tolist())
            data["text"].append("")

    for i in range(len(skeleton)):
        v = skeleton[i].vertex
        c = skeleton[i].confidence
        data["x"].append(v[0])
        data["y"].append(v[1])
        data["z"].append(v[2])
        data["color"].append(c + 1)
        data["size"].append(10)
        data["joints"].append(joint)
        data["vt"].append(vt.tolist())
        data["vn"].append(vn.tolist())
        data["text"].append(f"{KeypointEnum(i)}".replace("KeypointEnum.",""))

    fig = go.Figure(data=[go.Scatter3d(
            x=data["x"],
            y=data["y"],
            z=data["z"],
            text=data["text"],
            mode="markers+text",
            marker=dict(
                size=data["size"],
                color=data["color"],
                colorscale=[[0,"yellow"], [0.5,"blue"], [1,"red"]],
                opacity=0.8
            )
        )])

    fig.show()


def plotAllModels(allVerticesDict,cameras):
    data = []
    for c in range(len(cameras)):
        camera = cameras[c]
        verticesDict = allVerticesDict[camera]
        cameraData = {"x":[],"y":[],"z":[],"color":[],"joints":[],"size":[],"vt":[],"vn":[],"camera":camera}
        for i in range(len(verticesDict["v"])):
            v = verticesDict["v"][i]
            vt = verticesDict["vt"][i]
            vn = verticesDict["vn"][i]
            color = verticesDict["color"][i]
            joint = verticesDict["joints"][i]
            cameraData["x"].append(v[0])
            cameraData["y"].append(v[1])
            cameraData["z"].append(v[2])
            cameraData["color"].append(0 if joint == "" else verticesDict["confidence"][i]+1)
            cameraData["size"].append(5 if joint == "" else 10)
            cameraData["joints"].append(joint)
            cameraData["vt"].append(vt.tolist())
            cameraData["vn"].append(vn.tolist())
        data.append(cameraData)
    fig = make_subplots(
        rows=3, cols=2,
        specs=[[{"type": "scene"}, {"type": "scene"}],
               [{"type": "scene"}, {"type": "scene"}],
               [{"type": "scene"}, {"type": "scene"}]],
    )
    for i in range(len(cameras)):
        fig.add_trace(go.Scatter3d(
            x=data[i]["x"],
            y=data[i]["y"],
            z=data[i]["z"],
            text=data[i]["joints"],
            mode="markers+text",
            name=cameras[i],
            marker=dict(
                size=data[i]["size"],
                color=data[i]["color"],
                colorscale=[[0,"yellow"], [0.5,"blue"], [1,"red"]],
                opacity=0.8
            )
            ),
            row=int(i*0.5)+1, col=i%2+1)
    fig.update_layout(height=1800, showlegend=False)
    fig.show()

def plotKeypoints(keypoints):
    print(f"{col.Yellow}plot keypoints{col.ENDC}")
    data = []
    nKeypoints = int(len(keypoints)/3)
    for i in range(nKeypoints):
        data.append({
            'x':keypoints[i*3],
            'y':keypoints[i*3+1],
            'c':keypoints[i*3+2],
            'name':f"{KeypointEnum(i)}".replace("KeypointEnum.","")
            })
    plot = px.scatter(data,x="x",y="y",color="c",text="name")
    plot.show()

def prepareDirectories():
    # clean
    print(f"{col.Yellow}prepare directories{col.ENDC}")

    subprocess.run(["rm","-rf",settings["openposeImagesDir"]])
    subprocess.run(["rm","-rf",f"{settings['keypointsDir']}/*"])

    os.mkdir(settings["openposeImagesDir"])

    for file in glob.glob(f"{settings['modelDir']}/*Color.png"):
        shutil.copy(file, settings["openposeImagesDir"])
        print(f"{file} > {settings['openposeImagesDir']}")

    for file in glob.glob(f"{settings['openposeImagesDir']}/*.png"):
        subprocess.run(["convert",file,"-rotate",f"{settings['keypointsRotation']}",file])

def detectSkeletonKeypoints():
    print(f"{col.Yellow}detect skeleton keypoints{col.ENDC}")
    subprocess.run(["./processImages.sh"])

def intro():
    print("\n\nstudio")
    subprocess.run(["toilet","pointer*"])
    print("               presents:")
    print("                         M A K E   3 D   S K E L E T O N")

######### flow
def main():
    start = time.perf_counter()
    intro()
    prepareDirectories()
    detectSkeletonKeypoints()
    cameras = getCamerasFromDirectory(settings["modelDir"])
    allKeypoints = {}

    # get cameras and keypoints
    for i in range(len(cameras)):
        camera = cameras[i]
        keypoints = getKeypointsFromCamera(settings["keypointsDir"],camera)
        allKeypoints[camera] = keypoints

    verticesDict = getCameraVerticesDict(settings["modelDir"],cameras)

    skeletons = {}
    for i in range(len(cameras)):
        camera = cameras[i]
        skeleton = getSingleCameraSkeleton(allKeypoints[camera], verticesDict[camera], camera);
        skeletons[camera] = skeleton

    plotAllModels(verticesDict,cameras)

    ueberSkeleton = getUeberSkeleton(skeletons)
    end = time.perf_counter()
    duration = end - start

    pprint.pprint(ueberSkeleton)
    bvh = skeletonToBvh(ueberSkeleton)
    f = open("skeleton.bvh", "w")
    f.write(bvh)
    f.close()

    plotModelAndSkeleton(verticesDict,ueberSkeleton,cameras)

    print(f"{col.Red}{verticesDict}{col.ENDC}")

    print(f"{col.Blue}hooray, the main function has been executed in {duration} seconds{col.ENDC}")


main()
