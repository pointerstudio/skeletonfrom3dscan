import enum
import BVH

class KeypointEnum(enum.Enum):
    Nose = 0 
    Neck = 1 
    RShoulder = 2 
    RElbow = 3 
    RWrist = 4 
    LShoulder = 5 
    LElbow = 6 
    LWrist = 7 
    MidHip = 8 
    RHip = 9 
    RKnee = 10
    RAnkle = 11
    LHip = 12
    LKnee = 13
    LAnkle = 14
    REye = 15
    LEye = 16
    REar = 17
    LEar = 18
    LBigToe = 19
    LSmallToe = 20
    LHeel = 21
    RBigToe = 22
    RSmallToe = 23
    RHeel = 24
    Background = 25

def getDaughter(mother,skeleton,motherKeypointType,keypointType,nodeIndex,nodeFrame,end=False):
    daughter = BVH.BVHNode(KeypointEnum(keypointType),nodeIndex,nodeFrame)
    motherPosition = np.array([0,0,0]) if motherKeypointType < 0 else skeleton[motherKeypointType].vertex
    offset = skeleton[keypointType].vertex - np.array(mother.offset)
    daughter.offset = offset.tolist()
    daughter.chLabel = ["Zrotation","Yrotation","Xrotation"]
    if end:
        daughter.fHaveSite = True
    return daughter

def getOffset(skeleton,mom,kid):
    offset = skeleton[kid].vertex - skeleton[mom].vertex if mom > 0 else skeleton[kid].vertex
    return f"{round(offset[0],6)} {round(offset[1],6)} {round(offset[2],6)}"

def getName(kid):
    return f"{KeypointEnum(kid)}".replace("KeypointEnum.","")

def skeletonToBvh(skeleton):
    nodeIndex = 0
    nodeFrame = 0
    motion = []
    for i in range((5*3)+3):
       motion.append("0")

    bvh = f"""HIERARCHY
ROOT {getName(8)}
{{
	OFFSET {getOffset(skeleton,-1,8)}
	CHANNELS 6 Xposition Yposition Zposition Zrotation Yrotation Xrotation 
	JOINT {getName(1)}
	{{
		OFFSET {getOffset(skeleton,8,1)}
		CHANNELS 3 Zrotation Yrotation Xrotation 
		JOINT {getName(0)}
		{{
			OFFSET {getOffset(skeleton,1,0)}
			CHANNELS 3 Zrotation Yrotation Xrotation 
			JOINT {getName(15)}
			{{
				OFFSET {getOffset(skeleton,0,15)}
				CHANNELS 3 Zrotation Yrotation Xrotation 
				End Site
				{{
					OFFSET {getOffset(skeleton,15,17)}
				}}
			}}
			JOINT {getName(16)}
			{{
				OFFSET {getOffset(skeleton,0,16)}
				CHANNELS 3 Zrotation Yrotation Xrotation 
				End Site
				{{
					OFFSET {getOffset(skeleton,16,18)}
				}}
			}}
		}}
	}}
}}
MOTION
Frames: 1
Frame Time: .33
{" ".join(motion)}
"""

    # BVH.writeBVH("lol.bvh",root,,0,0)

    return bvh
