![preview](/preview.png)

# dependencies
- https://gitlab.com/pointerstudio/openpose-docker
- plotly-express `python -m pip install plotly-express`
- https://github.com/cacalabs/toilet (only necessary because we want a beautiful intro)

(optional but handy) bvh viewer:
http://www.cs.cmu.edu/~junggon/tools/gmov.html

# processImages
## prepare
adjust settings in `make3Dskeleton.py` and check the comments for what the script exactly expects

## run
`python make3Dskeleton.py`

TODO:
- full BVH skeleton output
