#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cmd="./build/examples/openpose/openpose.bin --image_dir /images --write_images /output --write_json /output --display 0"

mkdir -p output

docker run \
        --rm \
        -it \
        --gpus all \
        -v ${DIR}/images:/images \
        -v ${DIR}/output:/output \
        --name openpose \
        openpose \
        $cmd
